//
//  AppDelegate.swift
//  QueryForKiddom
//
//  Created by Raheel Ahmad on 5/12/18.
//  Copyright © 2018 Sakun Labs. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    let coordinator = AppCoordinator()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow()
        coordinator.launch(in: window)
        self.window = window
        return true
    }
}
