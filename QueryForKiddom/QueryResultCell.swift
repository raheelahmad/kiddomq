//
//  SearchResultCell.swift
//  QueryForKiddom
//
//  Created by Raheel Ahmad on 5/12/18.
//  Copyright © 2018 Sakun Labs. All rights reserved.
//

import UIKit
import Kingfisher

final class SearchResultCell: UITableViewCell {
    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        return label
    }()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        contentView.addSubview(thumbnailView)
        contentView.addSubview(label)

        thumbnailView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12.0).isActive = true
        thumbnailView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 12.0).isActive = true
        thumbnailView.widthAnchor.constraint(equalToConstant: 80.0).isActive = true
        thumbnailView.heightAnchor.constraint(equalToConstant: 80.0).isActive = true
        thumbnailView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -12.0).isActive = true

        label.leadingAnchor.constraint(equalTo: thumbnailView.trailingAnchor, constant: 12.0).isActive = true
        label.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -12.0).isActive = true
    }

    func configure(with result: QueryResult) {
        label.text = result.descriptionText
        thumbnailView.kf.setImage(with: URL(string: result.imageURLString))
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}
