//
//  QueryResult.swift
//  QueryForKiddom
//
//  Created by Raheel Ahmad on 5/12/18.
//  Copyright © 2018 Sakun Labs. All rights reserved.
//

import Foundation

struct QueryResult {
    let descriptionText: String
    let imageURLString: String
}

extension QueryResult {
    init?(json: JSON) {
        guard
            let result = json["result"] as? JSON,
            let descriptionText = result["description"] as? String,
            let imageDict = result["image"] as? JSON,
            let imageString = imageDict["contentUrl"] as? String
            else { return nil}
        self.descriptionText = descriptionText
        self.imageURLString = imageString
    }
}

typealias JSON = [String: Any]
