//
//  QueryEngine.swift
//  QueryForKiddom
//
//  Created by Raheel Ahmad on 5/12/18.
//  Copyright © 2018 Sakun Labs. All rights reserved.
//

import Foundation
import RxSwift

enum Result<T> {
    case success(T)
    case failure(reason: String)
}

extension Int {
    var isBadStatusCode: Bool {
        return self < 200 || self >= 400
    }
}

enum APIError: Error {
    case request
    case response
}

final class QueryEngine {
    let session = URLSession(configuration: .default)

    private let endpoint = "https://www.headlightlabs.com/api/assessment_search_wrapper"

    func search(query: String) -> Observable<[QueryResult]> {
        let requestURL = url(for: query)!
        return Observable.create { observer in
            let task = self.session.dataTask(with: requestURL) { data, response, error in
                if let error = error {
                    print("Request error: \(error.localizedDescription)")
                    observer.onError(APIError.response)
                    return
                }
                if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode.isBadStatusCode {
                    print("HTTP error status: \(httpResponse.statusCode)")
                    observer.onError(APIError.response)
                    return
                }
                guard let responseData = data, let responseJSON = try? JSONSerialization.jsonObject(with: responseData, options: []) as? JSON
                    else {
                        observer.onError(APIError.response)
                        return
                }
                guard
                    let response = responseJSON,
                    let results = response["itemListElement"] as? [JSON]
                else {
                    observer.onError(APIError.response)
                    return
                }
                let queryResults = results.compactMap({ QueryResult(json: $0) })
                observer.onNext(queryResults)
            }
            task.resume()
            return Disposables.create {
                task.cancel()
            }
        }
    }

    private func url(for query: String) -> URL? {
        let queryParam = URLQueryItem(name: "query", value: query)
        var urlComponents = URLComponents(string: endpoint)
        urlComponents?.queryItems = [queryParam]
        return urlComponents?.url
    }

}
