//
//  AppCoordinator.swift
//  QueryForKiddom
//
//  Created by Raheel Ahmad on 5/12/18.
//  Copyright © 2018 Sakun Labs. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol PreviousPresenter {
    func presentPreviousResults()
}

final class AppCoordinator: PreviousPresenter {
    let engine = QueryEngine()
    private let disposeBag = DisposeBag()

    lazy var mainViewController: MainViewController = {
        return MainViewController.make(engine: engine, presenter: self)
    }()

    func launch(in window: UIWindow) {
        window.rootViewController = mainViewController
        window.makeKeyAndVisible()
    }

    func presentPreviousResults() {
        let previousViewController = PreviousQueriesViewController(items: ["Cat", "Mouse"])
        let dismissBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "Cancel"), style: .plain, target: nil, action: nil)
        dismissBtn.rx.tap.bind {
            previousViewController.dismiss(animated: true, completion: nil)
        }.disposed(by: disposeBag)
        previousViewController.navigationItem.rightBarButtonItem = dismissBtn
        let nav = UINavigationController(rootViewController: previousViewController)
        mainViewController.present(nav, animated: true, completion: nil)
    }
}
