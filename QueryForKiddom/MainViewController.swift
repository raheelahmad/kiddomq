//
//  ViewController.swift
//  QueryForKiddom
//
//  Created by Raheel Ahmad on 5/12/18.
//  Copyright © 2018 Sakun Labs. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MainViewController: UIViewController {
    private let disposeBag = DisposeBag()
    static let cellId = String(describing: SearchResultCell.self)

    let searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.placeholder = "e.g., Cats"
        return searchBar
    }()

    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(SearchResultCell.self, forCellReuseIdentifier: cellId)
        return tableView
    }()

    let previousButton: UIButton = {
        let button = UIButton()
        let image = #imageLiteral(resourceName: "previous").withRenderingMode(.alwaysTemplate)
        button.setImage(image, for: .normal)
        button.tintColor = UIColor(red: 0.5, green: 0.2, blue: 0.3, alpha: 1.0)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white

        view.addSubview(searchBar)
        view.addSubview(tableView)
        view.addSubview(previousButton)
        setupConstraints()
    }

    private func setupConstraints() {
        searchBar.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        searchBar.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        searchBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        previousButton.trailingAnchor.constraint(equalTo: searchBar.trailingAnchor, constant: -12.0).isActive = true
        previousButton.centerYAnchor.constraint(equalTo: searchBar.centerYAnchor).isActive = true
    }
}

extension MainViewController {

    /// Sets up the main UIViewController with search
    ///
    /// - Parameter engine: The query engine
    /// - Returns: The view controller
    static func make(engine: QueryEngine, presenter: PreviousPresenter) -> MainViewController {
        let vc = MainViewController()

        // Bind the search query to the query engine,
        // and then back to the table.
        vc.searchBar.rx.text.orEmpty
            .asObservable()
            .skip(1)
            .debounce(1.0, scheduler: MainScheduler.instance)
            .flatMap { engine.search(query: $0) }
            .observeOn(MainScheduler.instance)
            .asObservable()
            .bind(to: vc.tableView.rx.items(cellIdentifier: MainViewController.cellId, cellType: SearchResultCell.self))
            { _, item, cell in
                cell.configure(with: item)
            }.disposed(by: vc.disposeBag)

        vc.previousButton.rx.tap
            .subscribe(onNext: { _ in
                presenter.presentPreviousResults()
            })
            .disposed(by: vc.disposeBag)

        return vc
    }
}
