# Query for Kiddom

This app queries the Google Knowledge Graph API and presents the results in a table view.

Libraries used:

- RxSwift for bindings
- Kingfisher for image fetches and caching

## Setup

Run `carthage install --platform iOS`.